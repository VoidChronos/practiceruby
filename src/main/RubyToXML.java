package main;

import org.jrubyparser.CompatVersion;
import org.jrubyparser.Parser;
import org.jrubyparser.ast.*;
import org.jrubyparser.parser.ParserConfiguration;
import org.w3c.dom.Document;

import org.w3c.dom.Element;

import java.io.Reader;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by Artem on 7/20/2015.
 */

public class RubyToXML {

    public static Document Convert(Reader rubySource, String rootNodeName) {
        DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder icBuilder;
        try {
            icBuilder = icFactory.newDocumentBuilder();
            Document doc = icBuilder.newDocument();
            Element rootElem = doc.createElement(rootNodeName);
            doc.appendChild(rootElem);
            iterateDoc(doc, rootElem, RubyToNode(rubySource));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static org.jrubyparser.ast.Node RubyToNode(Reader rubyFile) {
        Parser rubyParser = new Parser();
        CompatVersion version = CompatVersion.RUBY1_8;
        ParserConfiguration config = new ParserConfiguration(0, version);
        return rubyParser.parse("<code>", rubyFile, config);
    }

    private static String getNodeName(org.jrubyparser.ast.Node node)
    {
        String name = node.getClass().getName();
        int i = name.lastIndexOf('.');
        String nodeType = name.substring(i + 1);
        return nodeType;
    }

    private static String getNodeString(org.jrubyparser.ast.Node node) {
        /* This code is copy-pasted */
        StringBuilder builder = new StringBuilder(60);
        builder.append(getNodeName(node));
        if(node instanceof INameNode) {
            builder.append(":").append(((INameNode)node).getName());
        }
        return builder.toString();
    }

    private static void iterateDoc(Document doc, Element elem, org.jrubyparser.ast.Node node) {
        String nodeString = getNodeString(node);
        Element newElem;
        if (nodeString.indexOf(':') != -1) {
            String[] sp = nodeString.split(":");
            nodeString = sp[0];
            String inameNodeString = sp[1];
            newElem = doc.createElement(nodeString);
            newElem.setAttribute("IName", inameNodeString);
        } else {
            newElem = doc.createElement(nodeString);
        }
        elem.appendChild(newElem);
        for (org.jrubyparser.ast.Node child : node.childNodes())
            iterateDoc(doc, newElem, child);
    }
}
