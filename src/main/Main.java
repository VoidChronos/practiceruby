package main;

import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("No folder path specified.");
            return;
        }

        String folderPath = args[0];
        if (!folderPath.endsWith(File.separator))
            folderPath += File.separator;

     final File folder = new File(folderPath);
        ArrayList<String> files = listFilesForFolder(folder);

        new File("xml").mkdir();

       for (String filename : files) {
            Path filePath = Paths.get(folderPath + filename);
            if (Files.exists(filePath))
            {
                try {
                    FileReader rubyFile = new FileReader(filePath.toFile());
                    Document doc = RubyToXML.Convert(rubyFile, filename);
                    System.out.println("Parsing " + filename);
                    Transformer transformer = TransformerFactory.newInstance().newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
                    DOMSource source = new DOMSource(doc);
                    Result file_out = new StreamResult(new File("xml" + File.separator + filename.substring(0, filename.lastIndexOf(".rb")) + ".xml"));
                    transformer.transform(source, file_out);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }
    }

    public static ArrayList<String> listFilesForFolder(final File folder) {
        ArrayList<String> result = new ArrayList<String>();
        try {
            for (final File fileEntry : folder.listFiles()) {
                if (!fileEntry.isDirectory() && fileEntry.getName().endsWith(".rb")) {
                    result.add(fileEntry.getName());
                }
            }
        } catch (NullPointerException e) {
            System.out.println("Folder is empty");
        }
        return result;
    }

}