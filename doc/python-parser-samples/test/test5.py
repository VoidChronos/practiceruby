
class M():
    pass

m = M()

def get_index(aa):
    return aa

class A:
    def aaa(self):
        pass

class B:
    def bbb(self):
        pass

    def aaa(self):
        pass

aa = A()


str1 = "1"
str2 = "2"
str3 = "3"

y = 3
s = str1 if y > 4 else str2 if y > 3 else str3
print(s)

with open("test5.py") as fff:
    p = 10
    print(fff)
    print(p)

print(p)
print(fff)

def function(a, b):
    return a + b

xx = A()
yy = B()


xx, yy = yy, xx

xx.bbb()
yy.aaa()

y1, y2, y3 = xx, yy, xx

y1.bbb()
y2.aaa()
y3.bbb()


class my_exception_base(Exception):
    pass

class my_exception(my_exception_base):
    def __init__(self, a):
        pass

aa.aaa()

try:
    print_try("in try section")
    if y > 3:
        raise IOError
    else:
        e = my_exception(A())
        raise e #my_exception(e)
except my_exception_base:
    aa.aaa()
    aa = B()
    print1("handle my_exception_base")
except (IOError, ValueError):
    print2("handle IOError or ValueError")
except:
    print3("handle all")
else:
    print4("else")
finally:
    print5("finally")

print_qwerty(100)
aa.aaa()
