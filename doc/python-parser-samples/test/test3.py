

class parent:
    def foofoo(self):
        print("parent.foofoo")

class parent2:
    def foofoo(self):
        print("parent2.foofoo")

class test(parent2):

    def bar():
        print("test.bar")

    def foo(self):
        pass
        print("self(test).foo")
        #bar()
        test.bar()

    def foofoo(self):
        super().foofoo()
        print("test.foofoo")

class child(parent, parent2, test):

    def foo(self):
        print("self(child).foo")
        super().foofoo()
        super(child , self).foofoo()
        super(test, self).foofoo()
    
    def qqq(self):
        print("self(child).qqq")
        test.foo(self)


t = child()
t.foo()


b = 10

ca = test()

if b < 1:
    pass
else:
    ca = child()
#ca.foo()

action = (lambda x: (lambda y: x + y))

sum = lambda x, y : (x + y)

command=(lambda:sys.stdout.write('Spam\n'))

def test_b(x, y, z = 22):
    pass


test_b(b, b, z = b)



def f1(param1, *param2):
    pass

def f2(param1, **param2):
    return param2[b]

Matrix = [[0 for x in xrange(5)] for x in xrange(5) if x % 2 == 0]

def get_index(aa):
    return aa

x = 1
y = 2
Matrix[x][get_index(y)] = 1
