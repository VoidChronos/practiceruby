

gluglu = 10

def test():
    global gluglu
    gluglu = gluglu + 1
    print("test: gluglu = " + str(gluglu))
    print(gluglu)
    gluglu = 0

class parent1:
    def foo(self):
        print("parent1.foo")

    def __init__(self): self.foo()

class parent2:
    def bar(self):
        print("parent2.bar")

    def __init__(self): self.bar()

class child(parent2):

    wwww = 100

    def bar1(self):
        pass

    def test(fff, a):
        fff.bar1()
        fff.a = a
        print(fff.a)
        fff.bar()
        global gluglu
        gluglu = 1 + gluglu
        print("gloabl gluglu = " + str(gluglu))
        a.b.c

    def bar(self):
        print("child.bar")

        def foo():
            pass

    def __init__(self):
        pass

c = child()
c.test(1).www
c.wwww


def tester(start):
    aaa = 1
    def nested(label):
        global state          # Globals don't have to exist yet when declared
        state = 0             # This creates the name in the module now
        print(label, state)
        def nested2():
            nonlocal aaa
            aaa += 1
            return aaa
    return nested
