# Pick a random number.
rno = rand(100) + 1
print "Your magic number is ", rno, "\n"

# Perform all sort of totally uselss test on it and report the results.
if rno % 2 == 1 then
    print "Ooooh, that's an odd number.\n"
else
    print "That's an even number.\n"
    if rno > 2 then
        print "It's not prime, BTW.\n"
    end
end

if rno > 50 
    print "That's more than half as big as it could be!\n"
elsif rno == 42
    print "That's the ultimate magic number!!!!\n"
elsif rno < 10
    print "That's pretty small, actually.\n"
else
    print "What a boring number.\n"
end

if rno == 100 then print "Gosh, you've maxxed out!\n" end

items = [ 'Mark', 12, 'goobers', 18.45 ]
for it in items
    print it, " "
end

class Fred
  
  # The initialize method is the constructor.  The @val is
  # an object value.
  def initialize(v)
    @val = v
  end

  # Set it and get it.
  def set(v)
    @val = v
  end

  def get
    return @val
  end
end

# Ruby classes are always unfinished works.  This does not
# re-define Fred, it adds more stuff to it.
class Fred 
  def inc
    @val += 1
  end
end